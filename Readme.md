### Unifi-Controller Docker

Simply run "docker compose up -d" to run this image.
All data relies persistent in "./unifi_data"

As the Controller is running in a Dockercontainer, it will propagate the wrong IP Address of the Docker Deamon assigned Interface. Otherwise the Devices may stay in the "Adoption" Phase forever ;)
You'll need to change this in the `system.properties` file. E.g.

```
[sebi@pi46 docker-unifi (master) ]$ diff unifi_data/data/system.properties  unifi_data/data/system.properties.bk                                                                     19c19
		< system_ip=192.168.46.100
		---
		> # system_ip=a.b.c.d 
```
